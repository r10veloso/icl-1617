package ast;

import ast.value.Closure;
import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTFun implements ASTNode {

	String parameter;
	ASTNode body;

	public ASTFun(String parameter, ASTNode body) {
		this.parameter = parameter;
		this.body = body;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new Closure(parameter, body, env);
	}
	
	@Override
	public void compile(CodeBlock code) {
	}

	@Override
	public String toString() {
		return "fun " + parameter + " => " + body.toString() + " end";
	}
	
}
