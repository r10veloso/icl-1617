package ast;
import compiler.CodeBlock;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTId implements ASTNode {

	String id;

	public ASTId(String id)
	{
		this.id = id;
	}

	public IValue eval(Environment<IValue> env) 
			throws UndeclaredIdentifierException, ExecutionErrorException { 
		return env.find(id); 
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void compile(CodeBlock code) {
		// needs an environment -> Add it to the compile method signature
		// env.find(id); -> returns a pair (jumps, offset)
		// crawls the static link for the number of jumps
		// get the value from the frame in the given offset
	}
	
}

